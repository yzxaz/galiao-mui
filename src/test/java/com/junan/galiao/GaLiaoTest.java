package com.junan.galiao;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author junan
 * @version V1.0
 * @className GaLiaoTest
 * @disc
 * @date 2020/4/25 23:27
 */
public class GaLiaoTest {

    @Test
    public void test1() {
        String encode = new BCryptPasswordEncoder().encode("123456");
        System.out.println(encode);
    }

}