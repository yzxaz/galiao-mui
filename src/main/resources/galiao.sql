/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : galiao

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 29/04/2020 15:25:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '尬聊号',
  `user_pwd` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `user_nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `user_avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `user_friends` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '好友列表',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1004 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1000, '$2a$10$yn1l9Njzxk9suMgB3XjGO.hG2rBOMHjPe3d.QRAPCGUz9qOxOyjIa', '尬聊测试1号', 'https://img-blog.csdnimg.cn/2020042814482368.png', '1000,1001,1003', '2020-04-25 23:32:23');
INSERT INTO `user` VALUES (1001, '$2a$10$yn1l9Njzxk9suMgB3XjGO.hG2rBOMHjPe3d.QRAPCGUz9qOxOyjIa', '尬聊测试2号', 'https://img-blog.csdnimg.cn/2020042814482368.png', '1001,1000', '2020-04-26 16:56:10');
INSERT INTO `user` VALUES (1002, '$2a$10$yn1l9Njzxk9suMgB3XjGO.hG2rBOMHjPe3d.QRAPCGUz9qOxOyjIa', '尬聊测试3号', 'https://img-blog.csdnimg.cn/2020042814482368.png', '1001,1000,1002', '2020-04-27 20:04:02');
INSERT INTO `user` VALUES (1003, '$2a$10$yn1l9Njzxk9suMgB3XjGO.hG2rBOMHjPe3d.QRAPCGUz9qOxOyjIa', '一只小安仔', 'https://avatar.csdnimg.cn/B/F/8/1_qq_41793394_1587965876.jpg', '1003,1000', '2020-04-27 20:04:47');

SET FOREIGN_KEY_CHECKS = 1;
