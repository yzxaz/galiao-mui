package com.junan.galiao;

import com.junan.galiao.netty.GaLiaoServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 尬聊启动类
 */
@SpringBootApplication
@MapperScan(basePackages = "com.junan.galiao.mapper")
public class GaLiaoApplication implements CommandLineRunner {

    private final GaLiaoServer gaLiaoServer;

    public GaLiaoApplication(GaLiaoServer gaLiaoServer) {
        this.gaLiaoServer = gaLiaoServer;
    }

    @Override
    public void run(String... args) throws Exception {
        gaLiaoServer.start();
    }

    /**
     * 后端启动口
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        SpringApplication.run(GaLiaoApplication.class, args);
    }
}
