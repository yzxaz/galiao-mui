package com.junan.galiao.mapper;

import com.junan.galiao.pojo.dbo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author junan
 * @since 2020-04-24
 */
public interface UserMapper extends BaseMapper<User> {

}
