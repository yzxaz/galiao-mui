package com.junan.galiao.web.controller;

import com.junan.galiao.pojo.dbo.User;
import com.junan.galiao.pojo.dto.Message;
import com.junan.galiao.service.UserService;
import org.springframework.web.bind.annotation.*;

/**
 * @author junan
 * @version V1.0
 * @className UserController
 * @disc
 * @date 2020/4/25 15:23
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 登录
     *
     * @param user
     * @return
     */
    @PostMapping("/login")
    public Message login(@RequestBody User user) {
        return userService.login(user);
    }

    /**
     * 获取好友信息
     *
     * @param userId 好友尬聊号
     * @return
     */
    @GetMapping("/{userId}")
    public Message userById(@PathVariable Long userId, @RequestParam("token") String token) {
        return userService.userById(userId, token);
    }

    /**
     * 查找陌生人
     *
     * @param userId 尬聊号
     * @return
     */
    @GetMapping("/search/{userId}")
    public Message strangerById(@PathVariable Long userId, @RequestParam("token") String token) {
        return userService.strangerById(userId, token);
    }

    /**
     * 添加好友
     *
     * @param friendId 对方尬聊号
     * @return
     */
    @PostMapping("/friend/{friendId}")
    public Message addFriend(@PathVariable Long friendId, @RequestParam("token") String token) {
        return userService.addFriend(friendId, token);
    }

}