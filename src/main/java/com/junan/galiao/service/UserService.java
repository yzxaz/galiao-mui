package com.junan.galiao.service;

import com.junan.galiao.pojo.dbo.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.junan.galiao.pojo.dto.Message;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author junan
 * @since 2020-04-24
 */
public interface UserService extends IService<User> {

    /**
     * 用户登录
     * @param user
     * @return
     */
    Message login(User user);

    /**
     * 通过尬聊号查询user
     * @param userId
     * @return
     */
    Message userById(Long userId, String token);

    /**
     * 查找陌生人
     * @param userId
     * @param token
     * @return
     */
    Message strangerById(Long userId, String token);

    /**
     * 添加好友
     * @param friendId
     * @param token
     * @return
     */
    Message addFriend(Long friendId, String token);

}
