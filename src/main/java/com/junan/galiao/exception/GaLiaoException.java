package com.junan.galiao.exception;

/**
 * @author junan
 * @version V1.0
 * @className GaLiaoException
 * @disc
 * @date 2020/4/24 23:55
 */
public class GaLiaoException extends RuntimeException {

    public GaLiaoException(String message) {
        super(message);
    }
}