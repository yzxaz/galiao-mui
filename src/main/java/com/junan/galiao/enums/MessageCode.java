package com.junan.galiao.enums;

public enum MessageCode {

    /**
     * 一切正常
     */
    OK(1),

    /**
     * 异常
     */
    FAIL(2),

    /**
     * 用户找不到
     */
    USER_NOT_FOUND(3);

    private final int value;

    MessageCode(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }
}
