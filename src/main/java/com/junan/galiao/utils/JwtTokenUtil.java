package com.junan.galiao.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.junan.galiao.pojo.dbo.User;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;

/**
 * @author junan
 * @version V1.0
 * @className JwtTokenUtil
 * @disc jwt生成token的工具类
 * @date 2020/4/25 21:25
 */
public class JwtTokenUtil {

    /**
     * token失效时间
     */
    private static final long EXPIRE_TIME = 24 * 60 * 60 * 1000;

    /**
     * token发行人
     */
    private static final String ISSUER = "galiao";

    /**
     * 私钥（生成token）【 `一只小安仔` base64 加密得到 】
     */
    private static final String SECRET = "5LiA5Y+q5bCP5a6J5LuU";

    /**
     * 加密的私钥
     */
    private static Algorithm ALG_SECRET;

    static {
        try {
            ALG_SECRET = Algorithm.HMAC256(SECRET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 产生一个token
     *
     * @return
     */
    public static String createToken(User user) {
        // 过期时间 （1天）
        LocalDateTime expireTime = LocalDateTime.now().plusDays(1);
        // 构建header
        HashMap<String, Object> header = new HashMap<>();
        header.put("typ", "JWT");     //加密方式：jwt
        header.put("alg", "HS256");   //加密算法：HMAC SHA256

        return JWT.create()
                .withHeader(header)   // 设置header
                .withIssuer(ISSUER)   // 发行人
                .withClaim("user", FastJsonUtil.toJSONString(user))    // 设置 user 信息
                .withExpiresAt(Date.from(expireTime.atZone(ZoneId.systemDefault()).toInstant())) // 设置过期时间
                .sign(ALG_SECRET);    // 设置私钥
    }

    /**
     * 验证token
     *
     * @param token
     * @return
     */
    public static boolean verifyToken(String token) {
        try {
            return DecodedJWTByToken(token) != null;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 验证token
     *
     * @param token
     * @return
     */
    public static DecodedJWT DecodedJWTByToken(String token) {
            return JWT.require(ALG_SECRET)
                    .withIssuer(ISSUER)
                    .build()
                    .verify(token);
    }

    /**
     * 获取 token 中保存的user信息
     * @param token
     * @return
     */
    public static User userInToken (String token) {
        DecodedJWT verify = DecodedJWTByToken(token);
        String user = verify.getClaim("user").asString();
        return FastJsonUtil.parseObject(user, User.class);
    }

}