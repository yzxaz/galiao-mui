package com.junan.galiao.pojo.dto;

import lombok.Data;

/**
 * @author junan
 * @version V1.0
 * @className Message
 * @disc 客户端服务端数据交互格式对象
 * @date 2020/4/24 21:57
 */
@Data
public class Message {

    /**
     * 操作类型
     */
    private String action;

    /**
     * 消息接受者 (发送时对方尬聊号)
     */
    private Long receiver;

    /**
     * 消息发送者者 (发送时对方尬聊号)
     */
    private Long sender;

    /**
     * 传输的数据内容 （可以为 json ）
     */
    private Object data;

    /**
     * 状态码
     */
    private int code;

    /**
     * 登录的token
     */
    private String token;

    /**
     * 设置操作类型
     * @param action
     * @return
     */
    public Message action(String action) {
        this.action = action;
        return this;
    }

    /**
     * 设置数据
     * @param o
     * @return
     */
    public Message data(Object o) {
        this.data = o;
        return this;
    }

    /**
     * 设置状态码
     * @param code
     * @return
     */
    public Message code(int code) {
        this.code = code;
        return this;
    }

    /**
     * 设置Token
     * @param token
     * @return
     */
    public Message token(String token) {
        this.token = token;
        return this;
    }

    /**
     * 设置发送人
     * @param sender
     * @return
     */
    public Message sender(Long sender) {
        this.sender = sender;
        return this;
    }
}