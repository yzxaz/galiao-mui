package com.junan.galiao.pojo.dbo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.sql.Timestamp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.junan.galiao.serializer.TimestampFormatSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author junan
 * @since 2020-04-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    /**
     * 尬聊号
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 密码
     */
    private String userPwd;

    /**
     * 昵称
     */
    private String userNickname;

    /**
     * 头像地址
     */
    private String userAvatar;

    /**
     * 好友列表
     */
    private String userFriends;

    /**
     * 创建时间
     */
    @JsonSerialize(using =TimestampFormatSerializer.class)
    private Timestamp createTime;

    /**
     * 好友
     */
    @TableField(exist = false)
    private List<User> friends = new ArrayList<>();

    /**
     * 标志是否是当前登录用户的好友
     */
    @TableField(exist = false)
    private Boolean isFriend = false;

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

}
