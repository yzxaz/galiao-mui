# galiao-mui

#### 介绍
尬聊APP前端项目
![登录](https://images.gitee.com/uploads/images/2020/0429/162722_8f2cbc7f_4992631.jpeg "在这里输入图片标题")
![聊天](https://images.gitee.com/uploads/images/2020/0429/163024_a0c94adf_4992631.jpeg "在这里输入图片标题")
![二维码](https://img-blog.csdnimg.cn/20200429135658396.jpg "在这里输入图片标题")

#### 软件架构
前端项目主要使用 mui + vue + axios


#### 安装教程

1.  git clone https://gitee.com/junanaa/galiao-mui.git
2.  使用 hbuilder 打开项目
3.  打开 js -> config.js 修改后端 ip 和端口
4.  usb 连接 真机，hbuilder 真机运行。

#### 使用说明

1.  部分手机部分ui页面可能不兼容，我的小米8运行没问题。
2.  运行之前请先运行后端项目，否则无法登录。
3.  注意后端和手机在一个局域网才能访问，否则发生无法登录问题。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
